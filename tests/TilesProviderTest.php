<?php
namespace TheGame\Tests;

use PHPUnit\Framework\TestCase;
use TheGame\Tile;
use TheGame\TilesProvider;

class TilesProviderTest extends TestCase
{
    public function testCrateTile()
    {
        $provider = new TilesProvider();

        $this->assertInstanceOf(Tile::class, $provider->createTile());
    }
}
