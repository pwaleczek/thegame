<?php
namespace TheGame\Tests;

use PHPUnit\Framework\TestCase;
use TheGame\Board;
use TheGame\Tile;
use TheGame\TilesProvider;

class BoardTest extends TestCase
{
    private $board;

    public function setUp()
    {
        $this->board = new Board(
            new TilesProvider()
        );
    }

    public function testCreateEmptyBoardType()
    {
        $expected = array(
            array(new Tile(), new Tile()),
            array(new Tile(), new Tile())
        );

        $this->board->createEmptyBoard(2);

        $this->assertAttributeEquals($expected, 'board', $this->board);
    }

    public function testRandomizeWinner()
    {
        $this->board->createEmptyBoard(1);
        $this->board->randomizeWinner();

        $tile = new Tile();
        $tile->setWinning();
        $expected = array(array($tile));

        $this->assertAttributeEquals($expected, 'board', $this->board);
    }

    public function testRevealWinningTile()
    {
        $this->board->createEmptyBoard(1);
        $this->board->randomizeWinner();

        $this->assertTrue($this->board->revealTile(0, 0));
    }

    public function testRevealEmptyTile()
    {
        $this->board->createEmptyBoard(1);

        $this->assertAttributeEquals(0, 'tilesRevealed', $this->board);
        $this->assertFalse($this->board->revealTile(0, 0));
        $this->assertAttributeEquals(1, 'tilesRevealed', $this->board);
    }

    public function testRevealTileTwice()
    {
        $this->board->createEmptyBoard(2);

        $this->assertAttributeEquals(0, 'tilesRevealed', $this->board);
        $this->board->revealTile(0, 0);
        $this->board->revealTile(0, 0);
        $this->assertAttributeEquals(1, 'tilesRevealed', $this->board);
    }

    public function testGetNumberOfRevealedTiles()
    {
        $this->board->createEmptyBoard(2);

        $this->assertEquals(0, $this->board->getNumberOfRevealedTiles());
        $this->board->revealTile(0, 0);
        $this->assertEquals(1, $this->board->getNumberOfRevealedTiles());
        $this->board->revealTile(0, 1);
        $this->assertEquals(2, $this->board->getNumberOfRevealedTiles());
        $this->board->revealTile(0, 1);
        $this->assertEquals(2, $this->board->getNumberOfRevealedTiles());
    }
}
