<?php

namespace TheGame\Tests;

use PHPUnit\Framework\TestCase;
use TheGame\Tile;

class TileTest extends TestCase
{
    /**
     * @var Tile
     */
    private $tile;

    public function setUp()
    {
        $this->tile = new Tile();
    }

    public function testIsWinningDefault()
    {
        $this->assertFalse($this->tile->isWinning());
    }

    public function testSetWinning()
    {
        $this->tile->setWinning();

        $this->assertTrue($this->tile->isWinning());
    }

    public function testIsRevealedDefault()
    {
        $this->assertFalse($this->tile->isRevealed());
    }

    public function testSetRevealed()
    {
        $this->tile->setRevealed();

        $this->assertTrue($this->tile->isRevealed());
    }

    /**
     * @expectedException TheGame\Exceptions\OperationNotPermittedException
     */
    public function testSetRevealedTwice()
    {
        $this->tile->setRevealed();
        $this->tile->setRevealed();
    }
}
