<?php
namespace TheGame\Tests;

use PHPUnit\Framework\TestCase;
use TheGame\Board;
use TheGame\Exceptions\OperationNotPermittedException;
use TheGame\Game;
use TheGame\Tile;
use TheGame\TilesProvider;

class GameTest extends TestCase
{
    public function testConstruct()
    {
        $board = new Board(new TilesProvider());
        $interval = new \DateInterval('PT5S');

        $game = new Game($board, $interval, 3);

        $this->assertAttributeEquals($board, 'board', $game);
        $this->assertAttributeEquals($interval, 'gameInterval', $game);
        $this->assertAttributeEquals(3, 'maxMoves', $game);
        $this->assertAttributeEquals(Game::STATUS_SETUP, 'status', $game);
    }

    public function testStartNewGame()
    {
        $board = $this->getMockBuilder(Board::class)
            ->setConstructorArgs(array(new TilesProvider()))
            ->setMethods(array('createEmptyBoard', 'randomizeWinner'))
            ->getMock();

        $board->expects($this->once())
            ->method('createEmptyBoard')
            ->with($this->equalTo(5));

        $board->expects($this->once())
            ->method('randomizeWinner');

        $interval = new \DateInterval('PT5S');

        $game = new Game($board, $interval, 3);
        $game->startNewGame();

        $this->assertAttributeInstanceOf(\DateTimeImmutable::class, 'startTime', $game);
        $this->assertAttributeEquals(Game::STATUS_IN_PROGRESS, 'status', $game);
    }

    public function testMaxMoves()
    {
        $tile = $this->getMockBuilder(Tile::class)
            ->setMethods(array('isWinning', 'isRevealed'))
            ->getMock();

        $tile->method('isWinning', '')
            ->willReturn(false);
        $tile->method('isRevealed', '')
            ->willReturn(false);

        $provider = $this->getMockBuilder(TilesProvider::class)
            ->setMethods(array('createTile'))
            ->getMock();

        $provider->method('createTile')
            ->willReturn($tile);

        $board = new Board($provider);
        $interval = new \DateInterval('PT5S');
        $game = new Game($board, $interval, 3);
        $game->startNewGame();

        $game->revealTileAt(2, 0);

        $game->revealTileAt(2, 1);
        $this->assertEquals(Game::STATUS_IN_PROGRESS, $game->refreshStatus());

        $game->revealTileAt(2, 2);
        $this->assertEquals(Game::STATUS_LOST, $game->refreshStatus());
    }

    public function testMaxTime()
    {
        $tile = $this->getMockBuilder(Tile::class)
            ->setMethods(array('isWinning'))
            ->getMock();

        $tile->method('isWinning')
            ->willReturn(false);

        $provider = $this->getMockBuilder(TilesProvider::class)
            ->setMethods(array('createTile'))
            ->getMock();

        $provider->method('createTile')
            ->willReturn($tile);

        $board = new Board($provider);
        $interval = new \DateInterval('PT1S');
        $game = new Game($board, $interval, 3);
        $game->startNewGame();

        $this->assertEquals(Game::STATUS_IN_PROGRESS, $game->refreshStatus());

        // Should be avoided in tests but used for simplifying the model
        sleep(1);

        $this->assertEquals(Game::STATUS_LOST, $game->refreshStatus());

    }

    public function testGameWon()
    {
        $tile = $this->getMockBuilder(Tile::class)
            ->setMethods(array('isWinning'))
            ->getMock();

        $tile->method('isWinning')
            ->willReturn(true);

        $provider = $this->getMockBuilder(TilesProvider::class)
            ->setMethods(array('createTile'))
            ->getMock();

        $provider->method('createTile')
            ->willReturn($tile);

        $board = new Board($provider);
        $interval = new \DateInterval('PT1S');
        $game = new Game($board, $interval, 3);
        $game->startNewGame();

        $game->revealTileAt(0, 0);

        $this->assertEquals(Game::STATUS_WON, $game->refreshStatus());
    }

    /**
     * @expectedException \TheGame\Exceptions\OperationNotPermittedException
     */
    public function testMovesAfterGameOver()
    {
        $board = new Board(new TilesProvider());
        $interval = new \DateInterval('PT1S');
        $game = new Game($board, $interval, 1);
        $game->startNewGame();

        $game->revealTileAt(0, 0);
        $game->revealTileAt(1, 0);
    }

    /**
     * @expectedException \TheGame\Exceptions\OperationNotPermittedException
     */
    public function testMovesBeforeStart()
    {
        $board = new Board(new TilesProvider());
        $interval = new \DateInterval('PT1S');
        $game = new Game($board, $interval, 1);

        $game->revealTileAt(1, 0);
    }
}
