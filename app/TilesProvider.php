<?php
namespace TheGame;

class TilesProvider
{
    /**
     * Creates and returns a new Tile object
     *
     * @return Tile
     */
    public function createTile(): Tile
    {
        return new Tile();
    }
}
