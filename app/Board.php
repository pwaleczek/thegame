<?php
declare(strict_types=1);

namespace TheGame;

use TheGame\Exceptions\OperationNotPermittedException;

/**
 * Class Board. Handles the board and makes basic operations on tiles
 */
class Board
{
    /**
     * Size of the board
     *
     * @var int
     */
    protected $size;

    /**
     * Array of Tiles (2D)
     *
     * @var array
     */
    protected $board = array();

    /**
     * Amount of tiles already revealed on the board
     *
     * @var int
     */
    protected $tilesRevealed = 0;

    /**
     * @var TilesProvider
     */
    private $tilesProvider;

    /**
     * Board constructor.
     *
     * @param TilesProvider $tilesProvider Tiles provider object
     */
    public function __construct(TilesProvider $tilesProvider)
    {
        $this->tilesProvider = $tilesProvider;
    }

    /**
     * Reveals specified tile and returns true if it was the winning one, false otherwise
     *
     * @param int $row    Row number
     * @param int $column Column number
     *
     * @return bool
     */
    public function revealTile(int $row, int $column): bool
    {
        try {
            $tile = $this->getTile($row, $column);
            $tile->setRevealed();
            $this->tilesRevealed++;
        } catch (\OutOfBoundsException $e) {
            return false;
        } catch (OperationNotPermittedException $e) {
            return false;
        }

        return $tile->isWinning();
    }

    /**
     * Returns amount of currently revealed tiles
     *
     * @return int
     */
    public function getNumberOfRevealedTiles(): int
    {
        return $this->tilesRevealed;
    }

    /**
     * Crates an empty board using specified size
     */
    public function createEmptyBoard($size): void
    {
        $this->size = $size;

        for ($row = 0; $row < $this->size; ++$row) {
            $this->board[$row] = array();

            for ($col = 0; $col < $this->size; ++$col) {
                $this->board[$row][$col] = $this->tilesProvider->createTile();
            }
        }
    }

    /**
     * Pick one of tiles on the board and sets it as the winning one
     */
    public function randomizeWinner(): void
    {
        $this->getRandomTile()->setWinning();
    }

    /**
     * Returns a random tile from the board
     *
     * @return Tile
     */
    protected function getRandomTile(): Tile
    {
        $row = $this->getRandomCoord();
        $col = $this->getRandomCoord();

        return $this->getTile($row, $col);
    }

    /**
     * Generates random coordinate
     *
     * @return int
     */
    protected function getRandomCoord(): int
    {
        return rand(0, $this->size - 1);
    }



    /**
     * Gets a tile at specified row and column
     *
     * @param int $row    Row number (0-indexed)
     * @param int $column Column number (0-indexed)
     *
     * @return Tile
     */
    protected function getTile(int $row, int $column): Tile
    {
        $this->validateCoordinate($row);
        $this->validateCoordinate($column);

        return $this->board[$row][$column];
    }

    /**
     * Throws an exception if the value is out of the board
     *
     * @param int $coord One of the coordinates (column or row number - 0-based)
     *
     * @throws \OutOfBoundsException
     */
    private function validateCoordinate(int $coord): void
    {
        if ($coord < 0 || $coord >= $this->size) {
            throw new \OutOfBoundsException('Board coordinate out of bounds');
        }
    }
}
