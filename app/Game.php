<?php
declare(strict_types=1);

namespace TheGame;

use TheGame\Exceptions\GameOverException;
use TheGame\Exceptions\OperationNotPermittedException;

/**
 * Class Game. It contains current game state and allows to play
 */
class Game
{
    /**
     * Moves are still available
     */
    const STATUS_SETUP = 0;

    /**
     * Moves are still available
     */
    const STATUS_IN_PROGRESS = 1;

    /**
     * Current game was lost
     */
    const STATUS_LOST = 2;

    /**
     * Current game was won
     */
    const STATUS_WON = 3;

    /**
     * Board of tiles
     *
     * @var Board
     */
    private $board;

    /**
     * Time when the game was started
     *
     * @var \DateTimeImmutable
     */
    private $startTime;

    /**
     * Maximum game time interval
     *
     * @var \DateInterval
     */
    private $gameInterval;

    /**
     * Maximal amount of moves available
     *
     * @var int
     */
    private $maxMoves;

    /**
     * Current game status
     *
     * @var int
     */
    private $status = self::STATUS_SETUP;

    /**
     * Game constructor. Begins a new game
     *
     * @param Board         $board    Board object prepared to play
     * @param \DateInterval $gameTime Maximum time in seconds to find the winning tile
     * @param int           $maxMoves Maximum amount of available moves to find the winning tile
     *
     * @throws \Exception
     */
    public function __construct(Board $board, \DateInterval $gameTime, int $maxMoves = 5)
    {
        $this->board = $board;
        $this->gameInterval = $gameTime;
        $this->maxMoves = $maxMoves;
    }

    /**
     * Starts a new game
     *
     * @return void
     */
    public function startNewGame(): void
    {
        $this->board->createEmptyBoard(5);
        $this->board->randomizeWinner();

        $this->status = self::STATUS_IN_PROGRESS;
        $this->startTime = new \DateTimeImmutable();
    }

    /**
     * Updates and returns code of current game status (according to Game::STATUS_* constants)
     * @return int
     */
    public function refreshStatus(): int
    {
        try {
            $this->validate();
        } catch (OperationNotPermittedException $e) {
            // do nothing - this is just status update
        }

        return $this->status;
    }

    /**
     * Reveals one of board tiles specified by its coordinates.
     *
     * @param int $row    Row number (0-based)
     * @param int $column Column number (0-based)
     *
     * @return void
     */
    public function revealTileAt(int $row, int $column): void
    {
        if ($this->validate() && $this->board->revealTile($row, $column)) {
            $this->status = self::STATUS_WON;
        }

        $this->refreshStatus();
    }

    /**
     * Validates current game and updates the status. Returns true if game is still in progress, false otherwise
     *
     * @return bool
     *
     * @throws OperationNotPermittedException
     */
    protected function validate(): bool
    {
        try {
            $this->checkStatus();
            $this->checkMovesLimit();
            $this->checkTimer();

            return true;
        } catch (GameOverException $e) {
            $this->status = self::STATUS_LOST;
            return false;
        }
    }

    /**
     * @throws OperationNotPermittedException
     */
    private function checkStatus(): void
    {
        if ($this->status !== self::STATUS_IN_PROGRESS) {
            throw new OperationNotPermittedException('Game not in progress');
        }
    }

    /**
     * @throws GameOverException
     */
    private function checkTimer(): void
    {
        if ($this->startTime->add($this->gameInterval) < new \DateTime()) {
            throw new GameOverException('Out of time');
        }
    }

    /**
     * @throws GameOverException
     */
    private function checkMovesLimit(): void
    {
        if ($this->board->getNumberOfRevealedTiles() >= $this->maxMoves) {
            throw new GameOverException('Out of moves');
        }
    }
}
