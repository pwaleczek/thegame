<?php
declare(strict_types=1);

namespace TheGame;

use TheGame\Exceptions\OperationNotPermittedException;

/**
 * Class Tile. This class represents a single tile and it's state.
 */
class Tile
{
    /**
     * Determines if this is the tile which wins the game.
     *
     * @var bool
     */
    private $isWinning = false;

    /**
     * Determine if this tile has been already revealed or not
     * @var bool
     */
    private $isRevealed = false;

    /**
     * Returns true if this is the winning time, false otherwise
     *
     * @return bool
     */
    public function isWinning(): bool
    {
        return $this->isWinning;
    }

    /**
     * Sets the tile as a winning one
     */
    public function setWinning(): void
    {
        $this->isWinning = true;
    }

    /**
     * Returns true if this tile is already revealed
     *
     * @return bool
     */
    public function isRevealed(): bool
    {
        return $this->isRevealed;
    }

    /**
     * Reveals the tile. If it was already done it throws an exception
     *
     * @throws OperationNotPermittedException
     */
    public function setRevealed(): void
    {
        if ($this->isRevealed()) {
            throw new OperationNotPermittedException('Cannot reveal the same tile again');
        }

        $this->isRevealed = true;
    }
}
